ARG PHP_VERSION

FROM php:$PHP_VERSION-alpine

ARG DEPLOYER_VERSION

MAINTAINER mikk150 <mikk150@gmail.com>

RUN apk update --no-cache \
    && apk add --no-cache \
        openssh-client

RUN curl -L https://deployer.org/releases/v$DEPLOYER_VERSION/deployer.phar > /usr/local/bin/deployer \
    && chmod +x /usr/local/bin/deployer

WORKDIR /project

CMD ["--version"]
